#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: SadieYuCN
# @Date:   Mon Sep  5 21:02:32 CST 2016

# NEC协议常用在频率为38kHz的红外设备上（也就是说亮和灭的最小时间是26.3µs）。协议很简单：
# 亮562.5µs，再灭562.5µs，一共1.125ms表示bit“0”
# 亮562.5µs，再灭1.6875ms，一共2.25ms表示bit”1”
# 当一个按键被按下的时候：
# 先亮9ms，再灭4.5ms，表示开始
# 16 bit的地址码，主要用于区分设备
# 8 bit控制码
# 8 bit控制码取反，用于校验
# 亮562.5µs，表示单次传输结束
# 所以NEC协议一共能传输32bit数据，即十六进制下8位数字(0xaaaaaaaa).

def getValue(value):
    if value in range(lBoundOn, rBoundOn):
        return NEC_ON
    elif value in range(lBoundOff, rBoundOff):
        return NEC_OFF
    elif value in range(lBound500, rBound500):
        return 562
    elif value in range(lBound1688, rBound1688):
        return 1688
    elif value in range(lBound35k, rBound45K):
        return 40000
    elif value in range(lBound95K, rBound98k):
        return 96000
    elif value in range(lBound2k, rBound2k) :
        return 2200
    else:
        return value

def filterByLine(line):
    line = line.replace(" ", "").strip()
    # 以 "," 分割成数组
    splitArray = line.split(",")
    # 第一个9000第二个4500标识nec协议
    lineRsult = ""
    first = int(splitArray[0])
    if first in range(lBoundOn, rBoundOn):
        # print(str(first))
        for j, item in enumerate(splitArray):
            value = getValue(int(item))
            if j == len(splitArray) - 1:
                lineRsult = lineRsult + str(value)
            else:
                lineRsult = lineRsult + str(value) + ","
        # print(lineRsult)
        return lineRsult
    else:
        # print(line)
        return line

NEC_ON = 9000
NEC_OFF = 4500

# 放宽时间间隔．调高误差值

# 562 - 26.3 = 535.7 约等于 534
# 562 + 26.3 = 588.3 约等于 588
lBound500 = 500
rBound500 = 600

# 1688 - 26.3 = 1661.7 约等于 1660
# 1688 + 26.3 = 1714.3 约等于 1720

lBound1688 = 1600
rBound1688 = 1800

lBound2k = 2000
rBound2k = 2400

# 偏差 估值
deviation = 500

rBoundOn = 9000 + deviation
lBoundOn = 9000 - deviation

rBoundOff = 4500 + deviation
lBoundOff = 4500 - deviation

# 35000~45000 = 40000
lBound35k = 35000
rBound45K = 45000

# 95000~98000 = 96000
lBound95K = 95000
rBound98k = 98000

with open("./file/irlist.txt", "r") as ins:
    irList = []
    for line in ins:
        # print(line)
        line = line.replace(" ", "").strip()
        filterLine = filterByLine(line)
        # print(filterLine)
        irList.append(filterLine)

    thefile = open("./file/result.txt", "w")
    with thefile:
        # clear output file
        # thefile.write(str(irList))
        for ele in irList:
            thefile.write("%s\n\n" % ele)
            print(ele)
        pass




