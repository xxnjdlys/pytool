#coding:utf-8
import re
import os
import sys
import hashlib

path = os.getcwd()

def logo():
    print("\n")
    print("============================================")
    print("=============  Powered by WKTV  ============")
    print("============================================")
    print("\n")

def md5():
    # os.path.join(fname)
    hash_md5 = hashlib.md5()

    # print(sys.path)
    fname = sys.argv[0]
    with open(fname, "r+") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

#打印 logo
logo()

apkFilePath = raw_input("Please drag apk file to current window: \n")

# print("path=" + apkFilePath.replace(r"'","") + "\n")

temp = os.popen(r'aapt d badging '+ apkFilePath).readline()
if len(temp) > 0:
    print('\n')
    start = temp.find('name=')
    end = temp.find(' platform')
    temp = temp[start : end]
    temp = temp.replace(r' ','\n')
    print(temp)
else:
    print('error')

temp = os.popen(r'aapt l '+ apkFilePath).read()
index = temp.find('META-INF/tmwya_')
if index == -1:
    print("channel=No channel in apk")
else:
    start = index + 15
    channel = temp[start:]
    print(r"channel="+ channel.strip())

# realPath = apkFilePath.replace(r"'","")
print("md5=" +md5())

temp = os.popen(r'aapt d xmltree '+ apkFilePath + ' AndroidManifest.xml').read()
if len(temp)> 0:
    temp1 = re.findall(r'(Raw: "(.+?)")' ,temp)

    for i in range(0,len(temp1)):
        if temp1[i][0].find(r'UMENG_CHANNEL') != -1:
            j = i
            print(r'channel :'+ temp1[j+1][1])

    reg = re.compile(r'[0-9a-z]{24}')
    if reg.search(temp):
        temp = reg.findall(temp)
        print(r'appkey=' + temp[0].strip() + "\n")
raw_input()
